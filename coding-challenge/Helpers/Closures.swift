//
//  Closures.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation

// MARK: - Typealiases

// Empty Result + Void Return
typealias EmptyResult<ReturnType> = () -> ReturnType

// Common
typealias VoidResult = EmptyResult<Void>
