//
//  ItunesSearchManager.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/3/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation
import RxSwift

class ItunesSearchManager: APIManager {
    
    static func getItunesSearch() -> Observable<ItunesSearchData> {
        return request(APIRouterNetwork.getItunesSearch)
    }
}
