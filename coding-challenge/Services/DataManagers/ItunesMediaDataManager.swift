//
//  ItunesMediaDataManager.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation
import RxSwift

struct ItunesMediaDataManager {
    let disposeBag = DisposeBag()
    
    func fetchMedia() -> Observable<[ItunesMediaInfo]> {
        return Observable.create { (observer) -> Disposable in

            let infoObjects = self.mediaInfos()
            
            if !infoObjects.isEmpty {
                observer.onNext(infoObjects)
            }
            
            let infoObjectsObservable = ItunesSearchManager.getItunesSearch()
            
            infoObjectsObservable.subscribe(onNext: { mediaObjects in
                self.saveMediaInfos(mediaObjects.results)
                observer.onNext(mediaObjects.results)
                observer.onCompleted()
            }).disposed(by: self.disposeBag)
            
            return Disposables.create()
        }
    }

    func mediaInfos() -> [ItunesMediaInfo] {
        return RealmItunesMediaManager().fetchMediaInfos()
    }
    
    func saveMediaInfos(_ mediaInfos: [ItunesMediaInfo]) {
        RealmItunesMediaManager().saveMediaInfos(mediaInfos)
    }
}
