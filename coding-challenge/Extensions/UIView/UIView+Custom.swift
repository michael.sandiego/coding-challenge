//
//  UIView+Custom.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/3/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import UIKit
extension UIView {
    
    func makeRoundedCornerViews() {
        self.layer.cornerRadius = 10
        self.clipsToBounds = true
    }
}
