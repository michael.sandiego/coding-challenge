//
//  MediaListCell.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import UIKit
import SkeletonView

class MediaListCell: UITableViewCell {
    var viewModel: MediaListCellViewModelProtocol! {
        didSet {
            setupVM()
        }
    }
    
    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
}


// MARK: - Setup

private extension MediaListCell {

    func setupVM() {
        guard viewModel != nil else { return }
        
        mediaImageView.sd_setImage(with: viewModel.imageURL, placeholderImage: R.image.imagePlaceholder())
        titleLabel.text = viewModel.title
        genreLabel.text = viewModel.genre
        priceLabel.text = viewModel.price
    }
}
