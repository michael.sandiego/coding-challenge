//
//  MediaListCellViewModelProtocol.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation

protocol MediaListCellViewModelProtocol {
    var imageURL: URL? { get }
    var title: String { get }
    var genre: String { get }
    var price: String { get }
}
