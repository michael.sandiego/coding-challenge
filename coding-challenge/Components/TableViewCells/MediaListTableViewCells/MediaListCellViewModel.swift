//
//  MediaListCellViewModel.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation

class MediaListCellViewModel {
    
    var mediaInfo: ItunesMediaInfo
    
    init(mediaInfo: ItunesMediaInfo) {
        self.mediaInfo = mediaInfo
    }
}

extension MediaListCellViewModel: MediaListCellViewModelProtocol {
    var imageURL: URL? { URL(string: mediaInfo.artworkUrl100) }
    var title: String { mediaInfo.trackName }
    var genre: String { mediaInfo.primaryGenreName }
    var price: String { "\(mediaInfo.currency) \(mediaInfo.trackPrice)" }
}
