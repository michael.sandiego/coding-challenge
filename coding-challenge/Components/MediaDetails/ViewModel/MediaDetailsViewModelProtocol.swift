//
//  MediaDetailsViewModelProtocol.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation

protocol MediaDetailsViewModelProtocol {
    var imageURL: URL? { get }
    var title: String { get }
    var genre: String { get }
    var releaseDate: String { get }
    
    var buyPriceHD: String? { get }
    var buyPrice: String? { get }
    var rentPriceHD: String? { get }
    var rentPrice: String? { get  }
    
    var description: String { get }
}
