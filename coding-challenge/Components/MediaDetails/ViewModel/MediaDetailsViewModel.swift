//
//  MediaDetailsViewModel.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation

class MediaDetailsViewModel {
    
    var mediaInfo: ItunesMediaInfo!
    
    init(mediaInfo: ItunesMediaInfo) {
        self.mediaInfo = mediaInfo
    }
}

extension MediaDetailsViewModel: MediaDetailsViewModelProtocol {
    var imageURL: URL? { URL(string: mediaInfo.artworkUrl100) }
    var title: String { mediaInfo.trackName }
    var genre: String { mediaInfo.primaryGenreName }
    
    var releaseDate: String {
        let dateFormatterGet = ISO8601DateFormatter()

        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "yyyy"

        let date: Date? = dateFormatterGet.date(from: mediaInfo.releaseDate)
        
        return dateFormatterPrint.string(from: date ?? Date())
    }
    
    var buyPriceHD: String? {
        if mediaInfo.trackHdPrice != nil {
            return "Buy HD \(mediaInfo.currency) \(mediaInfo.trackHdPrice ?? 0.0)"
        } else { return nil }
    }
    
    var buyPrice: String? {
        "Buy \(mediaInfo.currency) \(mediaInfo.trackPrice)"
    }
    
    var rentPriceHD: String? {
        if mediaInfo.trackHdRentalPrice != nil {
            return "Rent HD \(mediaInfo.currency) \(mediaInfo.trackHdRentalPrice ?? 0.0)"
        } else { return nil }
    }
    
    var rentPrice: String? {
        if mediaInfo.trackRentalPrice != nil {
            return "Rent \(mediaInfo.currency) \(mediaInfo.trackRentalPrice ?? 0.0)"
        } else { return nil }
    }
    
    var description: String {
        mediaInfo.longDescription
    }
}
