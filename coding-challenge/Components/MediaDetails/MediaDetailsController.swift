//
//  MediaDetailsController.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import UIKit
import SDWebImage

class MediaDetailsController: UIViewController {

    @IBOutlet weak var mediaImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var yearLabel: UILabel!
    @IBOutlet weak var genreLabel: UILabel!
    
    @IBOutlet weak var buyHDView: UIView!
    @IBOutlet weak var buyView: UIView!
    @IBOutlet weak var rentHDView: UIView!
    @IBOutlet weak var rentView: UIView!
    
    @IBOutlet weak var priceHDLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var rentHDPriceLabel: UILabel!
    @IBOutlet weak var rentPriceLabel: UILabel!
    
    @IBOutlet weak var descriptionLabel: UILabel!
    
    var primaryKey: Int? {
        didSet {
            configureData()
        }
    }
    
    private var viewModel: MediaDetailsViewModel? {
        didSet {
            setupVM()
        }
    }
    
    func configureData() {
        if let primaryKey = primaryKey {
            if let mediaInfo = RealmItunesMediaManager.shared.getObject(with: primaryKey) {
                self.viewModel = MediaDetailsViewModel(mediaInfo: mediaInfo)
            }
        }
    }
    
    // MARK: - View Controller Lifecycle
    
    // Used by our scene delegate to return an instance of this class from our storyboard.
    static func loadFromStoryboard() -> MediaDetailsController? {
        let mdc = R.storyboard.main.mediaDetailsController()
        return mdc
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupVM()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
       
        if #available(iOS 13.0, *) {
            view.window?.windowScene?.userActivity = nil
        }
    }
    
    private func setupVM() {
        if let viewModel = viewModel,
            let miv = mediaImageView,
            let tl = titleLabel,
            let yl = yearLabel,
            let gl = genreLabel,
            let dl = descriptionLabel {
            
            miv.sd_setImage(with: viewModel.imageURL, placeholderImage: R.image.imagePlaceholder())
            tl.text = viewModel.title
            
            yl.text = viewModel.releaseDate
            gl.text = viewModel.genre
            
            dl.text = viewModel.description
            
            customizeSubviews()
        }
    }
    
    private func customizeSubviews() {
        if let  bhdv = buyHDView,
            let bv = buyView,
            let rhdv = rentHDView,
            let rv = rentView,
            let pl = priceLabel,
            let phdl = priceHDLabel,
            let rpl = rentPriceLabel,
            let rphdl = rentHDPriceLabel {
            
        bhdv.makeRoundedCornerViews()
        bv.makeRoundedCornerViews()
        rhdv.makeRoundedCornerViews()
        rv.makeRoundedCornerViews()
                
        bhdv.isHidden = !(viewModel?.buyPriceHD != nil)
        bv.isHidden = !(viewModel?.buyPrice != nil)
        rhdv.isHidden = !(viewModel?.rentPriceHD != nil)
        rv.isHidden = !(viewModel?.rentPrice != nil)
        
        pl.text = viewModel?.buyPrice
        phdl.text = viewModel?.buyPriceHD
        rpl.text = viewModel?.rentPrice
        rphdl.text = viewModel?.rentPriceHD
        }
    }
}

// MARK: - NSUserActivity Support

extension MediaDetailsController {
    
    static let restoreActivityKey = "RestoreActivity"
    static let activityPrimaryKey = "primaryKey"

    class var activityType: String {
        let activityType = ""

        // Load our activity type from our Info.plist.
        if let activityTypes = Bundle.main.infoDictionary?["NSUserActivityTypes"] {
            if let activityArray = activityTypes as? [String] {
                return activityArray[0]
            }
        }
        return activityType
    }
    
    func applyUserActivityEntries(_ activity: NSUserActivity) {
        let itemPrimaryKey: [String: Int] = [MediaDetailsController.activityPrimaryKey: primaryKey!]
        activity.addUserInfoEntries(from: itemPrimaryKey)
    }

    // Used to construct an NSUserActivity instance for state restoration.
    var detailUserActivity: NSUserActivity {
        let userActivity = NSUserActivity(activityType: MediaDetailsController.activityType)
        userActivity.title = "Restore Item"
        applyUserActivityEntries(userActivity)
        return userActivity
    }
}

// MARK: - UIUserActivityRestoring

extension MediaDetailsController {
    
    override func updateUserActivityState(_ activity: NSUserActivity) {
        super.updateUserActivityState(activity)
        applyUserActivityEntries(activity)
    }

    override func restoreUserActivityState(_ activity: NSUserActivity) {
         super.restoreUserActivityState(activity)

        // Check if the activity is of our type.
        if activity.activityType == MediaDetailsController.activityType {
            // Get the user activity data.
            if let activityUserInfo = activity.userInfo {
                let primaryKeyObject = activityUserInfo[MediaDetailsController.activityPrimaryKey]
                self.primaryKey = primaryKeyObject as? Int
            }
        }
    }
}

// MARK: - State Restoration (UIStateRestoring)

extension MediaDetailsController {
    
    override func encodeRestorableState(with coder: NSCoder) {
        super.encodeRestorableState(with: coder)

        let encodedActivity = NSUserActivityEncoder(detailUserActivity)
        coder.encode(encodedActivity, forKey: MediaDetailsController.restoreActivityKey)
    }
   
    override func decodeRestorableState(with coder: NSCoder) {
        super.decodeRestorableState(with: coder)
        
        if coder.containsValue(forKey: MediaDetailsController.restoreActivityKey) {
            if let decodedObject = coder.decodeObject(forKey: MediaDetailsController.restoreActivityKey),
                let decodedActivity = decodedObject as? NSUserActivityEncoder {
                if let activityUserInfo = decodedActivity.userActivity.userInfo {
                    let objectPrimaryKey = activityUserInfo[MediaDetailsController.activityPrimaryKey]
                    self.primaryKey = objectPrimaryKey as? Int
                }
            }
        }
    }
}
