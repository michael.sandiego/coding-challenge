//
//  MediaListController.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import UIKit

class MediaListController: UITableViewController {

    var mediaDetailsController: MediaDetailsController?
    
    var viewModel = MediaListViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if let split = splitViewController {
            let controllers = split.viewControllers
            guard let navController = (controllers[controllers.count-1] as? UINavigationController),
                let topViewController = navController.topViewController
                else { return }
            mediaDetailsController = topViewController as? MediaDetailsController
        }
        
        setupView()
    }
    
    private func setupView() {
        viewModel.fetchItunesMedia()
        self.tableView.register(R.nib.mediaListCell)
        setupObservables()
    }
    
    private func setupObservables() {
        viewModel.onDataChanged = {
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
}
    
// MARK: - Segues
extension MediaListController {

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if let mediaDetails = R.segue.mediaListController.pushMediaDetails(segue: segue) {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let primaryKey = viewModel.getPrimaryKey(at: indexPath.row)
                mediaDetails.destination.primaryKey = primaryKey
            }
        }
    }
}

// MARK: - Table view
extension MediaListController {
    
     override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
   
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 50))
        let dateStampLabel = UILabel()
        dateStampLabel.frame = CGRect(x: 0,
                                      y: 0,
                                      width: headerView.frame.size.width,
                                      height: headerView.frame.size.height)
        dateStampLabel.font = UIFont.systemFont(ofSize: 15)
        dateStampLabel.textAlignment = .center
        dateStampLabel.backgroundColor = .gray
        dateStampLabel.textColor = .white
       
        if let dateStamp = DateStamp().getDateStamp() {
            dateStampLabel.text = "Last visit was \(dateStamp)"
        } else {
            dateStampLabel.text = "Hello! This is your first visit!"
        }
        headerView.addSubview(dateStampLabel)
       
        return headerView
    }
   
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let mediaListCellViewModel = viewModel.viewModel(at: indexPath.row)
        let cell = tableView.dequeueReusableCell(withIdentifier: R.reuseIdentifier.mediaListCell, for: indexPath)!
        cell.viewModel = mediaListCellViewModel
        return cell
    }

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: R.segue.mediaListController.pushMediaDetails, sender: self)
        self.tableView.deselectRow(at: indexPath, animated: false)
    }
}
