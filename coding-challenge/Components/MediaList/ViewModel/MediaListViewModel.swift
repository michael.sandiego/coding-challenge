//
//  MediaListViewModel.swift
//  coding-challenge
//
//  Created by Mike San Diego on 8/5/20.
//  Copyright © 2020 Mic San Diego. All rights reserved.
//

import Foundation
import RxSwift
import RealmSwift

class MediaListViewModel {
    
    let disposeBag = DisposeBag()
    
    var onDataChanged: VoidResult?
    
    private var itunesMediaInfos = [ItunesMediaInfo]()
    
    func fetchItunesMedia() {
        ItunesMediaDataManager().fetchMedia()
            .subscribe(onNext: { mediaInfos in
                self.itunesMediaInfos = mediaInfos
            }, onError: { error in
                print("error - \(error.localizedDescription)")
            }, onCompleted: {
                self.onDataChanged?()
            }).disposed(by: disposeBag)
    }
    
    func numberOfRows() -> Int {
        return itunesMediaInfos.count
    }
    
    func viewModel(at index: Int) -> MediaListCellViewModel {
        let mediaInfo = itunesMediaInfos[index]
        return MediaListCellViewModel(mediaInfo: mediaInfo)
    }

    func getPrimaryKey(at index: Int) -> Int {
        
        let mediaInfo = itunesMediaInfos[index]
        let primaryKey = mediaInfo.trackId
        return primaryKey
    }
}
